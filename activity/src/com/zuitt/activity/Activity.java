package com.zuitt.activity;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("First Name:");
        String firstName = userInput.nextLine();
        System.out.println("Last Name:");
        String lastName = userInput.nextLine();
        System.out.println("First Subject Grade:");
        Double firstGrade = new Double(userInput.nextLine());
        System.out.println("Second Subject Grade:");
        Double secondGrade = new Double(userInput.nextLine());
        System.out.println("Third Subject Grade:");
        Double thirdGrade = new Double(userInput.nextLine());
        System.out.println("Good day, " + firstName + " " + lastName + ".");
        Double finalGrade = (firstGrade + secondGrade + thirdGrade)/3;
        System.out.println("your grade average is: " + finalGrade);
    }
}
