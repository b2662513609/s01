package com.zuitt.example;

public class Variables {
    /* Primitive
    * - int for integer
    * - double for float
    * - char for a single character
    * - boolean for boolean values
    * Non-Primitive Data Types
    * - String
    * - Arrays
    * - Classes
    * - Interface
    * */
    public static void main(String[] args) {
        int age = 18;
        char middle_initial = 'V';
        boolean isLegalAge = true;
        System.out.println("The user age is " + age);
        System.out.println("The user middle initial is " + middle_initial);
        System.out.println("Is the user of legal age? " + isLegalAge);
    }
}
